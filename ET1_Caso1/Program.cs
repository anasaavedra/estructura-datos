﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET1_Caso1
{
    class Program
    {
        static void Main(string[] args)
        {
           
            Nodo nodo1 = new Nodo
            {
                nombres = "Maria Elena",
                apell = "Castro Viera",
                edad = 21,
                anterior=null,
                siguiente = null
            };
            ListaEnlazadaDoble empleados = new ListaEnlazadaDoble();
            empleados.cabeza = nodo1;
            empleados.AgregarFinal("Juan Carlos", "Ramos Gutierrez", 32);
            empleados.AgregarFinal("David", "Sandoval Lescano", 40);
            empleados.AgregarFinal("Diana Sofia", "Torres Viera", 25);
            empleados.AgregarFinal("Rodrigo", "Cruz Talledo", 51);
            Console.WriteLine("--- Lista inicial ---");
            empleados.RecorrerLista();

            Console.WriteLine("-----------------------------------");
            Console.WriteLine("---Agregando 2 nuevos empleados---");            
            empleados.AgregarFinal("Kiara","Benavides Rosas",28);           
            empleados.AgregarFinal("Katia","Suarez Vela",41);
            Console.WriteLine("--- Nueva lista ---");
            empleados.RecorrerLista();

            Console.WriteLine("-----------------------------");
            Console.WriteLine("---Eliminando 3 empleados---");            
            empleados.Eliminar("Castro Viera");                 
            empleados.Eliminar("Cruz Talledo");
            empleados.Eliminar("Sandoval Lescano");
            Console.WriteLine("--------------------------");
            empleados.RecorrerLista();
            Console.Read();

        }
    }
}
