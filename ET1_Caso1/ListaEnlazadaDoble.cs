﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET1_Caso1
{
    class ListaEnlazadaDoble
    {
        public Nodo cabeza;
        public void RecorrerLista()
        {
            Nodo actual = cabeza;
            int c = 1;
            while (actual != null)
            {
                Console.WriteLine("Empleado " + c + ": ");
                Console.WriteLine("Nombres y apellidos: "+actual.nombres + " "+ actual.apell);
                Console.WriteLine("Edad: "+actual.edad);
                Console.WriteLine();
                actual = actual.siguiente;
                c++;
            }
        }

       
        public void AgregarFinal(string nomb,string apell,int edad)
        {
            Console.WriteLine("Agregando al empleado: " + nomb + " " + apell);

            if (cabeza == null)
            {
                cabeza = new Nodo();
                cabeza.nombres = nomb;
                cabeza.apell = apell;
                cabeza.edad = edad;
                cabeza.siguiente = null;
                cabeza.anterior = null;
            }
            else
            {
                Nodo actual = cabeza;
                Nodo nuevo = new Nodo();
                nuevo.nombres = nomb;
                nuevo.apell = apell;
                nuevo.edad = edad;

                while (actual.siguiente != null)
                {
                    actual = actual.siguiente;
                }
                actual.siguiente = nuevo;
                nuevo.anterior = actual;
            }

        }
        
       
        public void Eliminar(string referencia)
        {
            Console.WriteLine("Eliminando empleado: " + referencia);
            Nodo temp,puntero = new Nodo();
            puntero = cabeza;

            while (puntero.apell != referencia)
            {
                puntero = puntero.siguiente;
            }               

            temp = puntero;
            if (puntero.anterior != null)
            {
                puntero.anterior.siguiente = puntero.siguiente;
            }
            else {
                cabeza = puntero.siguiente;
            }          
            temp = null;
        }

    }
}
