﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET1_Caso1
{
    class Nodo
    {
        public string nombres { get; set; }
        public string apell { get; set; }
        public int edad { get; set; }
        public Nodo siguiente { get; set; }
        public Nodo anterior { get; set; }

    }
}
