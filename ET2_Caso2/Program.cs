﻿using System;

namespace ET2_Caso2
{
    class Program
    {
        static void Main(string[] args)
        {
            ColaDePrioridad.Nodo cola1,aux;
            cola1 = ColaDePrioridad.NuevoNodo("N0021","Juan","Merino", 10);
            cola1 = ColaDePrioridad.push(cola1, "N0052","Mariela","Sánchez", 12);
            cola1 = ColaDePrioridad.push(cola1, "N0025", "Miguel", "Salinas", 15);
            cola1 = ColaDePrioridad.push(cola1, "N0034", "María", "Villegaz", 11);
            cola1 = ColaDePrioridad.push(cola1, "N0012", "Guillermo", "Tudela", 9);
            aux = cola1;

            Console.WriteLine("--- Lista de alumnos por prioridad ----");
            while (!ColaDePrioridad.isEmpty(aux))
            {
                Console.WriteLine("Alumno: " + ColaDePrioridad.top(aux));
                aux = ColaDePrioridad.pop(aux);
            }
            Console.WriteLine();

            Console.WriteLine("Agregando alumno con prioridad 0...");
            cola1 = ColaDePrioridad.push(cola1, "N0014", "Fiorella", "Torres", 0);

            Console.WriteLine("--- Lista de alumnos por prioridad ----");
            while (!ColaDePrioridad.isEmpty(cola1))
            {
                Console.WriteLine("Alumno: " + ColaDePrioridad.top(cola1));
                cola1 = ColaDePrioridad.pop(cola1);
            }

            Console.Read();
        }

        class ColaDePrioridad
        {
            public class Nodo
            {
                public int Prioridad;
                public string Id;
                public string Nombre;
                public string Apellido;               
                public Nodo Siguiente;
            }

            public static Nodo nodo = new Nodo();

            public static Nodo NuevoNodo(string id,string nombre,string apellido, int prioridad)
            {
                Nodo temp = new Nodo();
                temp.Id = id;
                temp.Nombre = nombre;
                temp.Apellido = apellido;
                temp.Prioridad = prioridad;
                temp.Siguiente = null;
                return temp;
            }

            public static string top(Nodo cabeza)
            {
                return (cabeza).Id + " "+(cabeza).Nombre +" "+(cabeza).Apellido;
            }
            public static Nodo pop(Nodo cabeza)
            {
                Nodo temp = cabeza;
                (cabeza) = (cabeza).Siguiente;
                return cabeza;
            }

            public static Nodo push(Nodo cabeza, string id,string nombre,string apellido, int prioridad)
            {
                Nodo puntero = (cabeza);
                Nodo temp = NuevoNodo(id,nombre,apellido, prioridad);
                if ((cabeza).Prioridad > prioridad)
                {
                    temp.Siguiente = cabeza;
                    (cabeza) = temp;
                }
                else
                {
                    while (puntero.Siguiente != null &&
                        puntero.Siguiente.Prioridad < prioridad)
                    {
                        puntero = puntero.Siguiente;
                    }
                    temp.Siguiente = puntero.Siguiente;
                    puntero.Siguiente = temp;
                }
                return cabeza;
            }

            public static bool isEmpty(Nodo cabeza)
            {
                if (cabeza == null)
                {
                    return true;
                }
                return false;
            }

        }
    }
}
