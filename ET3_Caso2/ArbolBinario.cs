﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET3_Caso2
{
    class ArbolBinario
    {
        public Nodo raiz;
        public Nodo GetRaiz()
        {
            return raiz;
        }

        public void Inorden(Nodo raiz)
        {
            if (raiz != null)
            {
                Inorden(raiz.izquierdo);
                Console.WriteLine("{0} -  {1} - {2}", raiz.Id, raiz.historiaCli,raiz.paciente);
                Inorden(raiz.derecho);
            }
        }

        public void Preorden(Nodo raiz)
        {
            if (raiz != null)
            {
                Console.WriteLine("{0} -  {1} - {2}", raiz.Id, raiz.historiaCli, raiz.paciente);
                Preorden(raiz.izquierdo);
                Preorden(raiz.derecho);
            }
        }

        public void Postorden(Nodo raiz)
        {
            if (raiz != null)
            {
                Postorden(raiz.izquierdo);
                Postorden(raiz.derecho);
                Console.WriteLine("{0} -  {1} - {2}", raiz.Id, raiz.historiaCli, raiz.paciente);
            }
        }

        public void InsertarNodo(int id, string hist,string paciente)
        {
            Nodo puntero;
            Nodo padre;
            Nodo nodo = new Nodo
            {
                Id = id,
                historiaCli = hist,
                paciente = paciente
            };
            if (raiz != null)
            {
                puntero = raiz;
                while (true)
                {
                    padre = puntero;
                     if (id < puntero.Id)
                    {
                        puntero = puntero.izquierdo;
                        if (puntero == null)
                        {
                            padre.izquierdo = nodo;
                            break;
                        }
                    }
                    else
                    {
                        puntero = puntero.derecho;
                        if (puntero == null)
                        {
                            padre.derecho = nodo;
                            break;
                        }
                    }
                }
            }
            else
            {
                raiz = nodo;
            }
        }
              
        public void BuscarPorLlave(int llave)
        {
            int contador = 0;
            Nodo puntero = raiz;
            while (puntero != null)
            {
                contador ++; 
                if (puntero.Id == llave)
                {
                    Console.WriteLine("Historia clínica {0} encontrada",puntero.Id);                    
                    Console.WriteLine("Paciente: {0} ", puntero.paciente);
                    Console.WriteLine("\nTotal de iteraciones:" + contador);
                    return;
                }
                else
                {
                    if (llave > puntero.Id)
                    {
                        puntero = puntero.derecho;
                    }
                    else
                    {
                        puntero = puntero.izquierdo;
                    }
                }
            }
            Console.WriteLine("No se encontró la llave");
            Console.WriteLine("Total de iteraciones:" + contador);
        }       
    }
}
