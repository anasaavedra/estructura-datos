﻿using System;

namespace ET3_Caso2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("-------- Demo - Historias clínicas --------");
            ArbolBinario arbol = new ArbolBinario();
            arbol.InsertarNodo(5, "Historia clínica 5", "Paciente 5");
            arbol.InsertarNodo(1, "Historia clínica 1", "Paciente 1");
            arbol.InsertarNodo(6, "Historia clínica 6", "Paciente 6");
            arbol.InsertarNodo(10, "Historia clínica 10", "Paciente 10");      
            arbol.InsertarNodo(11, "Historia clínica 11", "Paciente 11");
            arbol.InsertarNodo(15, "Historia clínica 15", "Paciente 15");

            arbol.BuscarPorLlave(15);
            Console.WriteLine();

            Console.WriteLine("----------------- Inorder ---------------");
            arbol.Inorden(arbol.GetRaiz());
            Console.WriteLine("----------------- Inorder ---------------");
            Console.WriteLine();

            arbol.BuscarPorLlave(15);

            Console.ReadLine();
        }
    }
}
