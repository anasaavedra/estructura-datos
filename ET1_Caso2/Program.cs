﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET1_Caso2
{
    class Program
    {
        static void Main(string[] args)
        {
            Nodo cabeza1 = new Nodo();
            cabeza1.indice = 20;
            cabeza1.valor = "20";
            ListaEnlazadaSimple lista = new ListaEnlazadaSimple();
            lista.cabeza = cabeza1;           
            lista.AgregarAlFinal(2, "2");
            lista.AgregarAlFinal(19, "19");
            lista.AgregarAlFinal(1, "1");
            Console.WriteLine("---- Lista inicial ----");
            lista.RecorrerLista();
            Console.WriteLine("---- Lista ordenada ----");
            lista.Ordenar();
            lista.RecorrerLista();
            Console.Read();
        }
    }
}
