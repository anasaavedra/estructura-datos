﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET1_Caso2
{
    class ListaEnlazadaSimple
    {
        public Nodo cabeza;
        public void Ordenar()
        {
            int t = 1, c = 1;
            Nodo actual = cabeza;
            while (actual.siguiente != null)
            {
                actual = actual.siguiente;
                c++;
            }
           
            int temp = 0;
            String temp1 = "";
           
            do
            {
                actual = cabeza;
                Nodo sig = actual.siguiente;
                while (actual.siguiente != null)
                {
                    if (actual.indice > sig.indice)
                    {
                        temp = actual.indice; 
                        temp1 = actual.valor;
                        actual.indice = sig.indice;
                        actual.valor = sig.valor;
                        sig.indice = temp;
                        sig.valor = temp1;
                        actual = actual.siguiente;
                        sig = sig.siguiente;
                    }
                    else 
                    {
                        actual = actual.siguiente;
                        sig = sig.siguiente;
                    }
                }
                t++;
            } while (t <= c);
        }
        public void AgregarAlFinal(int indice, string valor)
        {
            Console.WriteLine("Agregando el elemento " + valor);
            if (cabeza == null)
            {
                cabeza = new Nodo();
                cabeza.indice = indice;
                cabeza.valor = valor;
                cabeza.siguiente = null;
            }
            else
            {
                Nodo actual = cabeza;
                Nodo nuevo = new Nodo();
                nuevo.indice = indice;
                nuevo.valor = valor;
                while (actual.siguiente != null)
                {
                    actual = actual.siguiente;
                }
                actual.siguiente = nuevo;
            }
            Console.WriteLine("");
        }
        public void RecorrerLista()
        {            
            Nodo actual = cabeza;
            while (actual != null)
            {
                Console.WriteLine(actual.valor);
                actual = actual.siguiente;
            }            
        }

        
    }
}
