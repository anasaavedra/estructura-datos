﻿using System;
using System.Collections.Generic;

namespace ET2_Caso1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("---Lista de programas---");

            Stack<ClsPrograma> my_programs = new Stack<ClsPrograma>();
           
            // Agregar elementos usando push
            my_programs.Push(new ClsPrograma("Programa 1", "Suma"));
            my_programs.Push(new ClsPrograma("Programa 2", "Resta"));
            my_programs.Push(new ClsPrograma("Programa 3", "Suma"));
            my_programs.Push(new ClsPrograma("Programa 4", "Multiplicación"));
            my_programs.Push(new ClsPrograma("Programa 5", "Resta"));

            // Recorrer elementos
            foreach (var item in my_programs)
            {
                Console.WriteLine(item.Nombre + "-" + item.Tipo);
            }
            Console.WriteLine("");


            // Procesando los dos últimos elementos en llegar
            Console.WriteLine("Eliminando los dos primeros ...");
            my_programs.Pop();
            my_programs.Pop();

            foreach (var item in my_programs)
            {
                Console.WriteLine(item.Nombre + "-" + item.Tipo);
            }
            Console.WriteLine("");
            Console.ReadLine();
        }
    }
}
