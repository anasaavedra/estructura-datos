﻿using System;
using System.Collections.Generic;

namespace EF_Caso1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("----------Demo - Delivery----------");
            Console.WriteLine("Estructura de datos: Por orden de llegada\n");
            ColasPrioridad.Nodo cola1,aux;
            cola1 = ColasPrioridad.NuevoNodo(10, "Lomo saltado", "Juan Perez", 20, 1);
            cola1 = ColasPrioridad.push(cola1, 15, "Pizza americana - Grande", "María Velarde", 56, 2);
            cola1 = ColasPrioridad.push(cola1, 45, "Makis", "Miguel Torres", 34, 3);
            cola1 = ColasPrioridad.push(cola1, 52, "Pollos a la brasa", "José Benavides", 72, 4);
            cola1 = ColasPrioridad.push(cola1, 60, "Anticuchos", "Luis Pinedo", 16, 5);
            aux = cola1;

            while (!ColasPrioridad.isEmpty(aux))
            {
                Console.WriteLine("{0} ", ColasPrioridad.top(aux));
                aux = ColasPrioridad.pop(aux);
            }
            Console.WriteLine("\nEstructura de datos: Árbol\n");
            ArbolBinario arbol = new ArbolBinario();
            arbol.InsertarNodo(10, "Lomo saltado", "Juan Perez", 20,1);
            arbol.InsertarNodo(15, "Pizza americana - Grande", "María Velarde", 56,2);
            arbol.InsertarNodo(45, "Makis", "Miguel Torres", 34,3);
            arbol.InsertarNodo(52, "Pollos a la brasa", "José Benavides", 72,4);
            arbol.InsertarNodo(60, "Anticuchos", "Luis Pinedo", 16,5);

            Console.WriteLine("-------------Inorder ----------");
            arbol.Inorden(arbol.GetRaiz());
            Console.WriteLine("------------ Inorder -----------");
            Console.WriteLine();

            Console.WriteLine("------------ Preorder -----------");
            arbol.Preorden(arbol.GetRaiz());
            Console.WriteLine("------------ Preorder -----------");
            Console.WriteLine();

            Console.WriteLine("---------- Postorder ----------");
            arbol.Postorden(arbol.GetRaiz());
            Console.WriteLine("---------- Postorder ----------");

            //Agregar pedido con prioridad
            Console.WriteLine("\nAgregando pedido con prioridad...\n");
            cola1 = ColasPrioridad.push(cola1, 19, "Arroz con leche", "María Santana", 5, 0);
            arbol.InsertarNodo(19, "Arroz con leche", "María Santana", 5, 0);

            while (!ColasPrioridad.isEmpty(cola1))
            {
                Console.WriteLine("{0} ", ColasPrioridad.top(cola1));
                cola1 = ColasPrioridad.pop(cola1);
            }

            Console.WriteLine("-------------Inorder ----------");
            arbol.Inorden(arbol.GetRaiz());
            Console.WriteLine("------------ Inorder -----------");
            Console.WriteLine();

            Console.WriteLine("------------ Preorder -----------");
            arbol.Preorden(arbol.GetRaiz());
            Console.WriteLine("------------ Preorder -----------");
            Console.WriteLine();

            Console.WriteLine("---------- Postorder ----------");
            arbol.Postorden(arbol.GetRaiz());
            Console.WriteLine("---------- Postorder ----------");

            Console.ReadLine();

        }
    }

    class ColasPrioridad
    {
        public class Nodo
        {
            public ClsPedido pedido;
            public Nodo siguiente;
        }

        public static Nodo nodo = new Nodo();

        public static Nodo NuevoNodo(int id, string pedido, string cliente, int costo, int prioridad)
        {
            Nodo temp = new Nodo();
            ClsPedido oPedido = new ClsPedido(id, pedido, cliente, costo, prioridad);
            temp.pedido = oPedido;            
            temp.siguiente = null;
            return temp;
        }

        public static string top(Nodo cabeza)
        {
            return (cabeza).pedido.Id + "- Pedido: " + (cabeza).pedido.Pedido + " Cliente:" + (cabeza).pedido.Cliente + " - Total a pagar: S/" + (cabeza).pedido.Costo;
        }

        public static Nodo pop(Nodo cabeza)
        {
            Nodo temp = cabeza;
            (cabeza) = (cabeza).siguiente;
            return cabeza;
        }

        public static Nodo push(Nodo cabeza, int id, string pedido, string cliente, int costo, int prioridad)
        {
            Nodo puntero = (cabeza);
            Nodo temp = NuevoNodo(id, pedido, cliente, costo, prioridad);
            if ((cabeza).pedido.Prioridad > prioridad)
            {
                temp.siguiente = cabeza;
                (cabeza) = temp;
            }
            else
            {
                while (puntero.siguiente != null &&
                    puntero.siguiente.pedido.Prioridad < prioridad)
                {
                    puntero = puntero.siguiente;
                }
                temp.siguiente = puntero.siguiente;
                puntero.siguiente = temp;
            }
            return cabeza;
        }

        public static bool isEmpty(Nodo cabeza)
        {
            if (cabeza == null)
            {
                return true;
            }
            return false;
        }
    }
}
