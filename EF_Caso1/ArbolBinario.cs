﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EF_Caso1
{
    class ArbolBinario
    {
        public Nodo raiz;

        public Nodo GetRaiz()
        {
            return raiz;
        }

        public void Inorden(Nodo raiz)
        {
            if (raiz != null)
            {
                Inorden(raiz.izquierdo);
                Console.WriteLine("{0} - Pedido: {1} Cliente: {2} - Total a pagar: S/ {3}",  raiz.pedido.Id, raiz.pedido.Pedido, raiz.pedido.Cliente, raiz.pedido.Costo);
                Inorden(raiz.derecho);
            }
        }

        public void Preorden(Nodo raiz)
        {
            if (raiz != null)
            {
                Console.WriteLine("{0} - Pedido: {1} Cliente: {2} - Total a pagar: S/ {3}", raiz.pedido.Id, raiz.pedido.Pedido, raiz.pedido.Cliente, raiz.pedido.Costo);
                Preorden(raiz.izquierdo);
                Preorden(raiz.derecho);
            }
        }

        public void Postorden(Nodo raiz)
        {
            if (raiz != null)
            {
                Postorden(raiz.izquierdo);
                Postorden(raiz.derecho);
                Console.WriteLine("{0} - Pedido: {1} Cliente: {2} - Total a pagar: S/ {3}", raiz.pedido.Id, raiz.pedido.Pedido, raiz.pedido.Cliente, raiz.pedido.Costo);
            }
        }

        public void InsertarNodo(int id, string pedido, string cliente, int costo,int prioridad)
        {
            Nodo puntero;
            Nodo padre;
            ClsPedido oPedido = new ClsPedido(id, pedido, cliente, costo,prioridad);
            Nodo nodo = new Nodo
            {
                pedido = oPedido
            };

            if (raiz != null)
            {
                puntero = raiz;
                while (true)
                {
                    padre = puntero;
                    if (id < puntero.pedido.Id)
                    {
                        puntero = puntero.izquierdo;
                        if (puntero == null)
                        {
                            padre.izquierdo = nodo;
                            break;
                        }
                    }
                    else
                    {
                        puntero = puntero.derecho;
                        if (puntero == null)
                        {
                            padre.derecho = nodo;
                            break;
                        }
                    }
                }
            }
            else
            {
                raiz = nodo;
            }
        }        
    }
}
