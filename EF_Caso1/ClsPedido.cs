﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EF_Caso1
{
    class ClsPedido
    {
        public int Id { get; set; }
        public int Prioridad { get; set; }
        public string Pedido { get; set; }
        public string Cliente { get; set; }
        public int Costo { get; set; }

        public ClsPedido(int id, string pedido, string cliente, int costo, int prioridad)
        {
            Id = id;
            Pedido = pedido;
            Cliente = cliente;
            Costo = costo;
            Prioridad = prioridad;
        }
    }
}
