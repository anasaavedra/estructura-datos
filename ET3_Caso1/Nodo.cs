﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET3_Caso1
{
    class Nodo
    {
        public int Id;
        public string Nombre;
        public string Apellido;
        public string Carrera;
        public Nodo izquierdo;
        public Nodo derecho;
    }
}
