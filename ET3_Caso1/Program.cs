﻿using System;

namespace ET3_Caso1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("---------------- Demo - Ingreso de postulantes --------------------");
            Random rNum = new Random();
            ArbolBinario arbolBi = new ArbolBinario();
            arbolBi.InsertarNodo(rNum.Next(1001), "Juan", "Castro Torres", "Medicina Humana");
            arbolBi.InsertarNodo(rNum.Next(1001), "Maria", "López Medina", "Ing. de Sistemas Computacionales");
            arbolBi.InsertarNodo(rNum.Next(1001), "Milena", "Roqueme Flores", "Ing. Industrial");
            arbolBi.InsertarNodo(rNum.Next(1001), "Miguel", "Huancas Rivera", "Administración");
            arbolBi.InsertarNodo(rNum.Next(1001), "Kathy", "Flores Vega", "Derecho");
           
            Console.WriteLine("-------------Inorder ----------");
            arbolBi.Inorden(arbolBi.GetRaiz());
            Console.WriteLine("------------ Inorder -----------");
            Console.WriteLine();

            Console.WriteLine("------------ Preorder -----------");
            arbolBi.Preorden(arbolBi.GetRaiz());
            Console.WriteLine("------------ Preorder -----------");
            Console.WriteLine();

            Console.WriteLine("---------- Postorder ----------");
            arbolBi.Postorden(arbolBi.GetRaiz());
            Console.WriteLine("---------- Postorder ----------");
           
            Console.ReadLine();

        }
    }
}
