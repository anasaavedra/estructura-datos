﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET3_Caso1
{
    class ArbolBinario
    {
        public Nodo raiz;

        public Nodo GetRaiz()
        {
            return raiz;
        }

        public void Inorden(Nodo raiz)
        {
            if (raiz != null)
            {
                Inorden(raiz.izquierdo);
                Console.WriteLine("{0} - {1} {2}        Carrera:{3}", raiz.Id, raiz.Nombre, raiz.Apellido, raiz.Carrera);
                Inorden(raiz.derecho);
            }
        }

        public void Preorden(Nodo raiz)
        {
            if (raiz != null)
            {
                Console.WriteLine("{0} - {1} {2}        Carrera:{3}", raiz.Id, raiz.Nombre, raiz.Apellido, raiz.Carrera);
                Preorden(raiz.izquierdo);
                Preorden(raiz.derecho);
            }
        }

        public void Postorden(Nodo raiz)
        {
            if (raiz != null)
            {
                Postorden(raiz.izquierdo);
                Postorden(raiz.derecho);
                Console.WriteLine("{0} - {1} {2}        Carrera:{3}", raiz.Id, raiz.Nombre, raiz.Apellido, raiz.Carrera);
            }
        }

        public void InsertarNodo(int id, string nombre,string apell,string carrera)
        {
            Nodo puntero;
            Nodo padre;
            Nodo nodo = new Nodo
            {
                Id = id,
                Nombre = nombre,
                Apellido=apell,
                Carrera=carrera
            };
            if (raiz != null)
            {
                puntero = raiz;
                while (true)
                {
                    padre = puntero;
                    if (id < puntero.Id)
                    {
                        puntero = puntero.izquierdo;
                        if (puntero == null)
                        {
                            padre.izquierdo = nodo;
                            break;
                        }
                    }
                    else
                    {
                        puntero = puntero.derecho;
                        if (puntero == null)
                        {
                            padre.derecho = nodo;
                            break;
                        }
                    }
                }
            }
            else
            {
                raiz = nodo;
            }
        }

        public void BuscarPorLlave(int llave)
        {
            Nodo puntero = raiz;
            while(puntero != null)
            {
                if (puntero.Id == llave)
                {
                    Console.WriteLine("Postulante {0} - {1} {2} encontrado", puntero.Id,puntero.Nombre,puntero.Apellido);
                    return;
                }
                else
                {
                    if (llave > puntero.Id)
                    {
                        puntero = puntero.derecho;
                    }
                    else
                    {
                        puntero = puntero.izquierdo;
                    }
                }
            }
            Console.WriteLine("No se encontró al postulante");
        }       
    }
}
